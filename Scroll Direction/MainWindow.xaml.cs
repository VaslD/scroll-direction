﻿using System;
using System.Collections.Generic;
using System.Security;
using System.Windows;
using System.Windows.Controls;

using Microsoft.Win32;

using Ookii.Dialogs.Wpf;

using ScrollDirection.Utilities;

namespace ScrollDirection
{
    public partial class MainWindow : Window
    {
        private RegistryKey _parent;

        public MainWindow()
        {
            InitializeComponent();
        }

        public void ShowErrorDialog(String heading, String body, Boolean allowElevation = false)
        {
            var dialog = new TaskDialog
                         {
                             AllowDialogCancellation = false,
                             MainIcon                = TaskDialogIcon.Error,
                             WindowTitle             = "Scroll Direction",
                             MainInstruction         = heading,
                             Content = body +
                                       (allowElevation
                                            ? Environment.NewLine +
                                              Environment.NewLine +
                                              "Try restart the application as administrator."
                                            : String.Empty),
                             ButtonStyle = allowElevation
                                               ? TaskDialogButtonStyle.CommandLinks
                                               : TaskDialogButtonStyle.Standard,
                         };

            if (allowElevation)
            {
                dialog.Buttons.Add(new TaskDialogButton
                                   {
                                       ButtonType        = ButtonType.Custom,
                                       Text              = "Run as administrator.",
                                       CommandLinkNote   = "Verification of login credentials may be required.",
                                       ElevationRequired = true,
                                       Default           = false
                                   });
                dialog.Buttons.Add(new TaskDialogButton
                                   {
                                       ButtonType = ButtonType.Custom,
                                       Text       = "Go back.",
                                       Default    = true
                                   });
            }
            else
            {
                dialog.Buttons.Add(new TaskDialogButton
                                   {
                                       ButtonType = ButtonType.Ok
                                   });
            }

            if (dialog.ShowDialog(this).Text.StartsWith("R", StringComparison.InvariantCultureIgnoreCase))
            {
                Hide();
                SecurityHelpers.RestartAsAdmin();
            }
        }

        public void ShowInfoDialog(String heading, String body)
        {
            var dialog = new TaskDialog
                         {
                             AllowDialogCancellation = false,
                             MainIcon                = TaskDialogIcon.Information,
                             WindowTitle             = "Scroll Direction",
                             MainInstruction         = heading,
                             Content                 = body,
                             ButtonStyle             = TaskDialogButtonStyle.Standard,
                         };
            dialog.Buttons.Add(new TaskDialogButton
                               {
                                   ButtonType = ButtonType.Close
                               });
            dialog.ShowDialog(this);
        }

        private void OnWindowLoaded(Object sender, RoutedEventArgs e)
        {
            try
            {
                using (_parent = Registry.LocalMachine.OpenSubKey("System")?.OpenSubKey("CurrentControlSet")
                                        ?.OpenSubKey("Enum")?.OpenSubKey("HID"))
                {
                    if (_parent == null) throw new InvalidOperationException("No HID device found.");

                    var mice = new List<Mouse>();
                    foreach (var sub in _parent.GetSubKeyNames())
                    {
                        var subKey = _parent.OpenSubKey(sub);

                        if (subKey == null) continue;

                        foreach (var device in subKey.GetSubKeyNames())
                        {
                            if (subKey.OpenSubKey(device) is RegistryKey deviceKey &&
                                String.Compare(deviceKey.GetValue("ClassGUID") as String,
                                               "{4D36E96F-E325-11CE-BFC1-08002BE10318}",
                                               StringComparison.InvariantCultureIgnoreCase) ==
                                0                                                                   &&
                                deviceKey.OpenSubKey("Device Parameters") is RegistryKey parameters &&
                                parameters.GetValue("FlipFlopWheel") is Int32 vDirection            &&
                                parameters.GetValue("FlipFlopHScroll") is Int32 hDirection)
                            {
                                mice.Add(new Mouse(sub, device, hDirection == 1, vDirection == 1));
                            }
                        }
                    }

                    HID.ItemsSource = mice;
                }
            }
            catch (SecurityException exception)
            {
                ShowErrorDialog("System.SecurityException",
                                exception.Message, true);
            }
            catch (InvalidOperationException exception)
            {
                ShowErrorDialog("System.InvalidOperationException",
                                exception.Message);
            }
        }

        private void OnHIDSelected(Object sender, SelectionChangedEventArgs e)
        {
            if (!(HID.SelectedItem is Mouse device)) return;

            if (device.HorizontalFlip)
            {
                NaturalH.IsChecked = true;
            }
            else
            {
                ReversedH.IsChecked = true;
            }

            if (device.VerticalFlip)
            {
                NaturalV.IsChecked = true;
            }
            else
            {
                ReversedV.IsChecked = true;
            }
        }

        private void OnNaturalHChecked(Object sender, RoutedEventArgs e)
        {
            ReversedH.IsChecked = false;
        }

        private void OnReversedHChecked(Object sender, RoutedEventArgs e)
        {
            NaturalH.IsChecked = false;
        }

        private void OnNaturalVChecked(Object sender, RoutedEventArgs e)
        {
            ReversedV.IsChecked = false;
        }

        private void OnReversedVChecked(Object sender, RoutedEventArgs e)
        {
            NaturalV.IsChecked = false;
        }

        private void OnSaveClicked(Object sender, RoutedEventArgs e)
        {
            if (!(HID.SelectedItem is Mouse device)) return;

            var flipH = NaturalH.IsChecked == true ? 1 : 0;
            var flipV = NaturalV.IsChecked == true ? 1 : 0;
            try
            {
                using (var parent = Registry.LocalMachine.OpenSubKey("System")?.OpenSubKey("CurrentControlSet")
                                           ?.OpenSubKey("Enum")?.OpenSubKey("HID")?.OpenSubKey(device.Parent)
                                           ?.OpenSubKey(device.Device)?.OpenSubKey("Device Parameters", true))
                {
                    if (parent == null) throw new InvalidOperationException("The registry key cannot be found.");

                    parent.SetValue("FlipFlopHScroll", flipH, RegistryValueKind.DWord);
                    parent.SetValue("FlipFlopWheel",   flipV, RegistryValueKind.DWord);
                }

                ShowInfoDialog("Scroll Direction Preferences Saved",
                               "FlipFlopHScroll: " +
                               flipH               +
                               Environment.NewLine +
                               "FlipFlopWheel: "   +
                               flipV);
            }
            catch (SecurityException exception)
            {
                ShowErrorDialog("System.SecurityException",
                                exception.Message, true);
            }
            catch (InvalidOperationException exception)
            {
                ShowErrorDialog("System.InvalidOperationException",
                                exception.Message);
            }
        }
    }
}
