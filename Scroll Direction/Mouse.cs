﻿using System;

namespace ScrollDirection
{
    internal class Mouse
    {
        public String  Parent         { get; }
        public String  Device         { get; }
        public Boolean HorizontalFlip { get; set; }
        public Boolean VerticalFlip   { get; set; }

        public Mouse(String parent, String device, Boolean horizontalFlip, Boolean verticalFlip)
        {
            Parent         = parent;
            Device         = device;
            HorizontalFlip = horizontalFlip;
            VerticalFlip   = verticalFlip;
        }

        public override String ToString() => $"HID\\{Parent}\\{Device}";
    }
}
