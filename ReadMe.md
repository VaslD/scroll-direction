# Scroll Direction

Get macOS scrolling on Windows.

This application enumerates mice available on a computer and allows user to change scroll direction per device. The change is done globally, on a device driver level.

No third-party driver/software installation required. Even if your device doesn't come with settings for scroll direction, this application can change it. However, third-party companion software (Options, CUE, etc.) may interfere, reverse or ignore changes done by this application.
